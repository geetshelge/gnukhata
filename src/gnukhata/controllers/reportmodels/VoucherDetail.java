package gnukhata.controllers.reportmodels;

public class VoucherDetail {
	private String voucherNo;
	private String voucherType;
	private String dateOfTransaction;
	private String drAccount;
	private String crAccount;
	private String amount;
	private String narration;
	private String projectName;
	private String voucherCode;
	private int lockflag;
	public VoucherDetail(String voucherNo, String voucherType,
			String dateOfTransaction, String drAccount, String crAccount,
			String amount, String narration, String voucherCode, String projectName, int lockflag) {
		
		this.voucherNo = voucherNo;
		this.voucherType = voucherType;
		this.dateOfTransaction = dateOfTransaction;
		this.drAccount = drAccount;
		this.crAccount = crAccount;
		this.amount = amount;
		this.narration = narration;
		this.projectName = projectName;
		this.voucherCode = voucherCode;
		this.lockflag = lockflag;
	}

	/**
	 * @return the vouvherNo
	 */
	public String getVoucherNo() {
		return voucherNo;
	}

	/**
	 * @return the voucherCode
	 */
	public String getVoucherCode() {
		return voucherCode;
	}

	/**
	 * @return the voucherType
	 */
	public String getVoucherType() {
		return voucherType;
	}

	/**
	 * @return the dateOfTransaction
	 */
	public String getDateOfTransaction() {
		return dateOfTransaction;
	}

	/**
	 * @return the drAccount
	 */
	public String getDrAccount() {
		return drAccount;
	}

	/**
	 * @return the craccount
	 */
	public String getCrAccount() {
		return crAccount;
	}

	/**
	 * @return the amount
	 */
	public String getAmount() {
		return amount;
	}

	/**
	 * @return the narration
	 */
	public String getNarration() {
		return narration;
	}

	/**
	 * @return the projectName
	 */
	public String getProjectName() {
		return projectName;
	}
	
	public int getlockFlag(){
		return lockflag;
	}

	/**
	 * @return the lockflag
	 */
	public int getLockflag() {
		return lockflag;
	}

	/**
	 * @param lockflag the lockflag to set
	 */
	public void setLockflag(int lockflag) {
		this.lockflag = lockflag;
	}
	
	

}
